const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let broadCastEmployeeSchema = new Schema({
    broadcast_id:Number,
    name: String,
    email: String,
    mobile: Number,
    emp_id: String,
    department: String,
}, {
    timestamps: true
}, {
    versionKey: false
})

let createBroadcastEmployeeModel = mongoose.model('broadcast_employee', broadCastEmployeeSchema)
createBroadcastEmployee = function (json, callback) {
    let broadcastEmployeejson = new createBroadcastEmployeeModel(json);
    broadcastEmployeejson.save((err, data) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, data)
        }
    })
}

module.exports = {
    createBroadcastEmployee,
    createBroadcastEmployeeModel
}
