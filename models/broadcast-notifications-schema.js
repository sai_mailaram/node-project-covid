const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let broadCastNotificationsSchema = new Schema({
    mobile: Number,
    email: String,
    channel: String,
    transaction_id:String,
    type: String,
    message: String,
    credits: String,
    character_count: Number,
}, {
    timestamps: true
}, {
    versionKey: false
})

let createBroadcastNotificationsModel = mongoose.model('broadcast_notifications', broadCastNotificationsSchema)
createBroadcastNotifications = function (json, callback) {
    let broadcastNotificationjson = new createBroadcastNotificationsModel(json);
    broadcastNotificationjson.save()
}

module.exports = {
    createBroadcastNotifications
}
