module.exports = {
    emailConfig : {
        apiKey: process.env.MAILGUN_SECRET,
        domain: process.env.MAILGUN_DOMAIN
    },
};
