const emailConfig = require('../config/secret-keys.config')['emailConfig'];
const mailgun = require('mailgun-js')(emailConfig);
var randomstring = require("randomstring");
const currentTime = require('./date.helper').currentTime;
const models = require('../models/broadcast-notifications-schema')

mailHelpers = {
    sendEmail: (recipient, message) =>
        new Promise((resolve, reject) => {
            const data = {
                from: 'Meltag <admin@meltag.com>',
                to: recipient,
                subject: message.subject,
                text: message.text,
                html: message.html,
            };

            mailgun.messages().send(data, (error) => {
                if (error) {
                    return reject(error);
                }
                return resolve();
            });

            for (var email of data.to) {
                let transactionId = randomstring.generate(10);
                mailHelpers.logResponse(email, message.text, transactionId, 'outbound');
            }
        }),

    logResponse: async (email, message, transactionId, type, channel = "email") => {

        await models.createBroadcastNotifications({
            email: email,
            transaction_id: transactionId,
            type: type,
            message: message,
            channel: channel,
            character_count: message.length,
            created_at: currentTime(),
            updated_at: currentTime()
        },(err, result) => {
            if (err) return err;
            else {
                return res.status(200).json({
                    status: "success",
                    data: result
                })
            }
        })
    }
}

module.exports = mailHelpers;
