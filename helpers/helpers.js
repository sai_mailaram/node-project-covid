const axios = require('axios');

helpers = {
    axiosCall : async (url, method, data = {}) => {

        return await axios({
            method: method,
            url: url,
            params: data
        });
    },
}

module.exports = helpers;
