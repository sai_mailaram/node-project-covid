const models = require('../models/broadcast-notifications-schema')
const axios = require('./helpers').axiosCall;
const currentTime = require('./date.helper').currentTime;

smsHelpers = {
    send: async (mobile, msg, language = 'English') => {
        new Promise(async (resolve, reject) => {
            const url = 'http://api.mVaayoo.com/mvaayooapi/MessageCompose';

            const params = {
                user: process.env.SMS_USER + ':' + process.env.SMS_PASSWORD,
                senderID: process.env.SMS_SENDERID || 'MELTAG',
                receipientno: mobile,
                msgtxt: msg,
            };

            if (language != 'English') {
                params = params.concat({
                    msgtype: 4,
                    dcs: 8,
                    ishex: 1,
                });
            }

            let response = await axios(url, 'GET', params);
            transactionId = response.data.trim().split(',')[1];

            smsHelpers.logResponse(mobile, msg, transactionId, 'outbound');
        })
    },

    logResponse: async (mobile, message, transactionId, type, channel = "sms") => {

        await models.createBroadcastNotifications({
            mobile: mobile,
            transaction_id: transactionId,
            type: type,
            message: message,
            channel: channel,
            credits: Math.ceil(message.length / 160),
            character_count: message.length,
            created_at: currentTime(),
            updated_at: currentTime()
        })
    }
}

module.exports = smsHelpers;
