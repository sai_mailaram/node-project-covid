const models = require('../models/broadcast-employee-schema.js')
const broadcastModel = require('../models/broadcast-schemas.js')
const Joi = require('@hapi/joi');

module.exports = {
    addEmployee: (req, res) => {
        let employeeDetails = req.body;
        var schema = module.exports.validations();

        const { error } =
            module.exports.validateData(schema, employeeDetails);
        var errorRows = [];
        if (error) {
            return res.status(400).json({
                status: false,
                comment: error.message,
            })

        }

        broadcastModel.createBroadcast({
            'file_name': 'others',
            'status': 'completed',
            'count': 1,
            'organization_id': 1
        });

        models.createBroadcastEmployee(employeeDetails, (err, result) => {
            if (err) return err;
            else {
                return res.status(200).json({
                    'message': "Employee Created",
                    status: "success",
                    data: result
                })
            }
        })

    },

    validations: function () {
        return Joi.object().keys({
            mobile: Joi.string().alphanum()
                .pattern(new RegExp('^[0-9]{10}$'))
                .messages({
                    "string.pattern.base": `Mobile must have 10 digits`,
                }).required(),
            name: Joi.string().required(),
            email:
                Joi.string()
                    .allow(null)
                    .email({
                        minDomainSegments: 2,
                        tlds: { allow: ['com', 'net'] }
                    }).required(),
            emp_id: Joi.string().required(),
            department: Joi.string().required(),
        });
    },
    validateData: (schema, data) => {
        return schema.validate({
            mobile: data.mobile,
            name: data.name,
            department: data.department,
            emp_id: data.emp_id,
            email: data.email,
        }, { abortEarly: false });
    },
}