// const models = require('../models/broadcast-schemas.js')
const models = require('../models/broadcast-employee-schema.js')
let csvParser = require('csv-parser');
let fs = require('fs');

module.exports = {
  createBroadcast: (req, res) => {
    let broadcastDetails = req.body;
    models.createBroadcast(broadcastDetails, (err, result) => {
      if (err) return err;
      else {
        return res.status(200).json({
          status: "success",
          data: result
        })
      }
    })
  },
  uploadfile: (req, res) => {
    // fs.createReadStream('data.csv')
    //   .pipe(csv())
    //   .on('data', (data) => results.push(data))
    //   .on('end', () => {
    //     console.log(results);
    //     // [
    //     //   { NAME: 'Daffy Duck', AGE: '24' },
    //     //   { NAME: 'Bugs Bunny', AGE: '22' }
    //     // ]
    //   });
    // var XLSX = require('xlsx')
    // var workbook = XLSX.readFile('data.xlsx');
    // var sheet_name_list = workbook.SheetNames;
    // var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
    // models.createBroadcast(xlData, (err, result) => {
    //     if (err) return err;
    //     else {
    //         return res.status(200).json({
    //             status: "success",
    //             data: result
    //         })
    //     }
    // })
    // res.send(xlData)
    res.send('file uploaded success')
  },

  getData: (req, res) => {
    models.createBroadcastEmployeeModel.aggregate([
      {
        $group: { _id: "$department", data: { $push: "$$ROOT" } }
      },

    ], (err, data) => {
      return res.status(200).json({
        status: 'success',
        data: data

      })
    })
  },

}