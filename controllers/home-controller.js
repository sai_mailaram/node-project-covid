const models = require('../models/model-schemas')
module.exports = {
    createUser: (req, res) => {
        let userDetails = req.body;
        models.createUser(userDetails, (err, result) => {
            if (err) return err;
            else {
                return res.status(200).json({
                    status: "success",
                    data: result
                })
            }
        })
    },

    updateUser: (req, res) => {
        let userName = req.body.userName;
        let empId = req.body.empId;
        models.createUserModel.findOneAndUpdate(
            { userName: userName },
            { empId: empId },
            (err, result) => {
                if (err) return err;
                return res.status(200).json({
                    status: "success",
                    message: 'Emp Id updated successfully',
                })
            })
    },

    deleteUser: (req, res) => {
        let userName = req.body.userName;
        models.createUserModel.findOneAndDelete(
            { userName: userName },
            (err, result) => {
                if (err) return err;
                return res.status(200).json({
                    status: "success",
                    message: 'User deleted successfully',
                })
            })
    }
}