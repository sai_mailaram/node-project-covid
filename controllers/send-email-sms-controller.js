const smsHelper = require('../helpers/sms.helper');
const mail = require('../helpers/mail.helper');
const basicHtml = require('../views/mails/basic-attachment.html');

module.exports = {
    sendSmsEmail: (req, res) => {
        module.exports.email(req);
        module.exports.sms(req);

        return res.status(200).json({
            "message" : "Email and Sms Sent Successfully",
            status: true,
        });

    },

    sendMail: (req, res) => {
        module.exports.email(req);
        return res.status(200).json({
            "message" : "Email Sent Successfully",
            status: true,
        });
    },

    sendSms: (req, res) => {
        module.exports.sms(req);
        return res.status(200).json({
            "message" : "Sms Sent Successfully",
            status: true,
        });
    },

    email: (req) => {
        let emails = req.body.email;
        let message = req.body.message;
        let subject = req.body.subject;

        mail.sendEmail(emails, {
            subject: subject,
            text: message,
            html: basicHtml
        });
    },
    sms: (req) => {
        let mobile_numbers = req.body.mobile_number;
        let message = req.body.message;

        for (let mobile_number of mobile_numbers) {
            smsHelper.send(mobile_number, message);
        }
    },

}