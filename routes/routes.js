const homeController = require('../controllers/home-controller.js')
const broadcastController = require('../controllers/broadcast-controller.js')
const sendSmsMailController = require('../controllers/send-email-sms-controller.js')
const addEmployeeController = require('../controllers/add-employee-controller.js')
module.exports = (app) => {
    app.post('/api/users/create', homeController.createUser)
    app.post('/api/users/update', homeController.updateUser)
    app.post('/api/users/delete', homeController.deleteUser)
    app.post('/api/broadcast/create', broadcastController.createBroadcast)
    // app.post('/api/broadcast/uploadfile', broadcastController.uploadfile)
    app.post('/api/broadcast/getData', broadcastController.getData)
    app.post('/api/send/sms', sendSmsMailController.sendSms)
    app.post('/api/send/email', sendSmsMailController.sendMail)
    app.post('/api/send/sms/email', sendSmsMailController.sendSmsEmail)
    app.post('/api/add/employee', addEmployeeController.addEmployee)
    var multer = require('multer')
    // const uploadss = require('./../uploads')
    var XLSX = require('xlsx')
    const models = require('../models/broadcast-employee-schema.js')
    var fs = require('fs')
    const csv = require('csv-parser');
    // const fs = require('fs');
    const path = require('path')

    var storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, './')
        },
        filename: (req, file, cb) => {
            nameFile = file.fieldname + '-' + Date.now() + path.extname(file.originalname)
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    const upload = multer({ storage: storage });
    app.post('/api/broadcast/uploadfile', upload.single('uploadfile'), async function (req, res) {
        console.log('storage location is ', req.hostname + '/' + req.file.path);
        console.log(nameFile)
        var workbook = await XLSX.readFile(req.file.path);
        var sheet_name_list = workbook.SheetNames;
        var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

        xlData.forEach(element => {
            models.createBroadcastEmployee(element, (err, result) => {
                if (err) return err;
                else {
                    return res.status(200).json({
                        status: "success",
                        data: result
                    })
                }
            })
        })
        // res.send(xlData)
        // res.send('file uploaded success')
        return res.send(req.file);
    })

}